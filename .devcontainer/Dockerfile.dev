FROM debian:stable-slim as base

# Tools needed by vscode
# trim down sources.list for faster operation \
RUN \
  export DEBIAN_FRONTEND="noninteractive" \
  && apt-get update \
  # basic tools for developer tooling \
  && apt-get install --no-install-recommends --yes jq wget curl unzip ca-certificates git openssh-client \
  && apt-get autoremove --yes \
  && apt-get clean --yes \
  && rm -rf /var/lib/{apt,dpkg,cache,log}/ \
  && echo "Done installing OS tools"

RUN echo "root-user: before JAVA_HOME: PATH $PATH"

ENV JAVA_HOME=/opt/java

COPY /build-tools/java_urls.env /root/java_urls.env

RUN --mount=type=cache,target=/tmp \
  . /root/java_urls.env \
  && echo "Installing JDK" \
  # Fetch the archive \
  && echo "File fetch url: [${JDK_FETCH_URL}]" \
  # download the file
  && FILENAME="/tmp/jdk.tar.gz" \
  && curl -sL -o ${FILENAME} "${JDK_FETCH_URL}" \
  # Validate the checksum \
  && echo "Downloaded successfully as ${FILENAME}" \
  && mkdir -p ${JAVA_HOME} \
  && echo "extract jdk into jvm path (${JAVA_HOME})" \
  && echo "tar -xzvf ${FILENAME} -C ${JAVA_HOME} --strip-components=1" \
  && tar -xzvf ${FILENAME} -C ${JAVA_HOME} --strip-components=1 \
  && rm -f ${FILENAME} \
  && echo "Done" \
  && unset FILENAME \
  && echo "Copy java executables to /usr/local/bin" \
  && ln -s ${JAVA_HOME}/bin/* /usr/local/bin/

RUN --mount=type=cache,target=/tmp \
  . /root/java_urls.env \
  && echo "Installing maven" \
  # Fetch the archive \
  && echo "File fetch url: [${MAVEN_URL}]" \
  # download the file
  && FILENAME="/tmp/maven.tar.gz" \
  && curl -sL -o ${FILENAME} "${MAVEN_URL}" \
  # Validate the checksum \
  && echo "Downloaded successfully as ${FILENAME}" \
  && MAVEN_INSTALL_DIR="/opt/maven" \
  && mkdir -p ${MAVEN_INSTALL_DIR} \
  && echo "extract maven into path (${MAVEN_INSTALL_DIR})" \
  && tar -xzvf ${FILENAME} -C ${MAVEN_INSTALL_DIR} --strip-components=1 \
  && rm -f ${FILENAME} \
  && ln -s ${MAVEN_INSTALL_DIR}/bin/ /usr/local/bin/ \
  && echo "Done" \
  && echo "Copy maven executables to /usr/local/bin" \
  && ln -s ${MAVEN_INSTALL_DIR}/bin/* /usr/local/bin/

# confirm installation
RUN \
  echo "Check if runtimes are installed correctly" \
  && echo "User: $(whoami)" \
  && echo "Check for correct JAVA_HOME[$JAVA_HOME]" \
  && [ ! -z ${JAVA_HOME} ] || exit 1 \
  && java --version \
  && mvn -v \
  && echo "Done verifying tools installations"

ENV USERNAME=vscode

RUN \
  USER_ID=9001 \
  && GROUP_ID=${USER_ID} \
  && addgroup --gid ${GROUP_ID} ${USERNAME} \
  && adduser --gid ${GROUP_ID} --uid ${USER_ID} --shell /bin/bash \
    --disabled-password --gecos "non-root user" ${USERNAME} \
  && echo "Done adding user"

USER ${USERNAME}

RUN echo "export PROMPT_COMMAND='history -a'" >> ${HOME}/.bashrc

RUN mkdir -p /home/${USERNAME}/.m2 /home/${USERNAME}/code \
  && chown -R ${USERNAME}:${USERNAME} /home/${USERNAME}

# add alias
RUN echo "alias c='cd ${HOME}/code'" >> ${HOME}/.bashrc

# confirm installation
RUN \
  echo "Check if runtimes are installed correctly" \
  && echo "User: $(whoami)" \
  && echo "Check for correct JAVA_HOME[$JAVA_HOME]" \
  && [ ! -z ${JAVA_HOME} ] || exit 1 \
  && java --version \
  && mvn -v \
  && echo "Done verifying tools installations"

VOLUME [ "/home/${USERNAME}/code" ]
VOLUME [ "/home/${USERNAME}/.m2" ]

WORKDIR "/home/${USERNAME}/code"

CMD echo "Started" && sleep infinity
