#!/bin/bash
mkdir certs

export DOMAIN_NAME="localhost"

openssl req -x509 -out ./certs/server.crt -keyout ./certs/server.key \
  -newkey rsa:2048 -nodes -sha256 \
  -subj "/CN=${DOMAIN_NAME}" -extensions EXT -config <( \
   printf "[dn]\nCN=${DOMAIN_NAME}\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:${DOMAIN_NAME}\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth")