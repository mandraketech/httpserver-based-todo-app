#!/bin/bash

hit_url() {
    unset HTTP_RESP_CODE
    url="$1"
    
    if [ -z ${url} ]; then
        >&2 echo "incorrect parameters to hit_url [${url}]"
        exit 100
    fi

    # Check the HTTP status code
    export HTTP_RESP_CODE=$(curl -sk -o /dev/null --no-buffer -w '%{http_code}' ${url})
    if [[ "${HTTP_RESP_CODE}" == "000" ]]; then
      export HTTP_RESP_CODE=0
    fi
    echo "Received http code:[$HTTP_RESP_CODE]"
}

check_abort_url() {
  msg="$1"
  url="$2"
  
  if [[ -z "${msg}" || -z "${url}" ]]; then
    >&2 echo "incorrect parameters to check_abort_url [${msg}] [${url}]"
    exit 100
  fi

  echo "$msg"
  # Check the HTTP status code
  hit_url ${url}

  # Fail if the response code is not 400-499 (HTTP aborts)
  if [[ ${HTTP_RESP_CODE} != 0 && (${HTTP_RESP_CODE} < 400 || ${HTTP_RESP_CODE} > 499) ]]; then
    >&2 echo "Error: Unexpected response code from [$url]: $HTTP_RESP_CODE"
    exit 10
  fi
}

check_redirect_url() {
  msg="$1"
  url="$2"
  
  if [ -z "${msg}" ] || [ -z "${url}" ]; then
    >&2 echo "incorrect parameters to check_redirect_url [${msg}] [${url}]"
    exit 100
  fi

  echo "$msg"
  # Check the HTTP status code
  hit_url ${url}

  # Fail if the response code is not 400-499 (HTTP aborts)
  if [[ $HTTP_RESP_CODE != 308 ]]; then
    >&2 echo "Error: Unexpected response code from [$url]: $HTTP_RESP_CODE"
    exit 11
  fi
}

check_success_or_gateway_error_url() {
  msg="$1"
  url="$2"
  
  if [[ -z "${msg}" || -z "${url}" ]]; then
    >&2 echo "incorrect parameters to check_success_or_gateway_error_url [${msg}] [${url}]"
    exit 100
  fi

  echo "$msg"
  # Check the HTTP status code
  hit_url ${url}

  # Fail if the response code is not 400-499 (HTTP aborts)
  if [[ $HTTP_RESP_CODE < 500 || $HTTP_RESP_CODE > 599 ]]; then
    >&2 echo "Error: Unexpected response code from [$url]: $HTTP_RESP_CODE"
    exit 12
  fi
}

check_redirect_url "Request to loadbalancer (raw)" "http://loadbalancer/"

check_abort_url "Request to loadbalancer (secure)" "https://loadbalancer/"

check_success_or_gateway_error_url "Request to external domain with environment variable [${EXTERNAL_DOMAIN}]" "https://${EXTERNAL_DOMAIN}/"