# To build this project

## Ensure JDK 21 is available
Either of:
- start in the devcontainer provided
- have valid JDK 21 installation

## Build and test using maven wrappers:
```bash
./mvnw clean compile
```
##  Run the test
```bash
./mvnw test
```

## Build, test and run
```bash
./mvnw compile test exec:java
```

## Build a "fat" jar, and run the tests
```bash
./mvnw package -Pprod
```

## Run the fat jar
- copy the "jar-with-dependencies" 
- run `java -jar <filename>.jar` 

# Generate new Java project

Run the following on the terminal if you are starting a Java project from scratch:

```bash
if [[ ! -f pom.xml ]]; then
    mvn archetype:generate -DgroupId=com.example -DartifactId=sample -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false -DarchetypeVersion=1.4
    mv sample/* .
    rmdir sample
fi
```

# Trim down Maven Wrapper directory

To trim down the maven wrapper directory, use the following approach. 
It will remove the dependency on the jar file

```bash

if [[ ! -d .mvnw || -f $CODE_DIR/.mvn/wrapper/maven-wrapper.jar ]]; then
    echo "Regenerating maven wrapper files"
    rm -rf mvnw mvnw.cmd .mvnw
    mvn wrapper:wrapper -Dtype=only-script
fi
```

# Initial version

The initial version of the project scripts were taken from: `https://gitlab.com/mandraketech/java-vscode-dev`
