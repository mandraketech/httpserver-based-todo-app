#!/usr/bin/env bash
get_arch() {    
    unset ARCH
    OS_ARCH="$(uname -m)"
    if [ "${OS_ARCH}" = "x86_64" ]; then
        export ARCH="x64"
    elif [ "${OS_ARCH}" = "aarch64" ] || [ "${OS_ARCH}" = "arm64" ]; then
        export ARCH="arm64"
    else
        echo "Unsupported architecture"
        exit 1
    fi

    
}

get_maven_url() {
    export MAVEN_VERSION=$(curl -s https://maven.apache.org/download.cgi | grep -o 'Downloading Apache Maven [0-9].[0-9].[0-9]' | grep -o '[0-9].[0-9].[0-9]')
    echo "Maven version: [$MAVEN_VERSION]"
    if [[ -z $MAVEN_VERSION ]]; then
        echo "Maven version not detected"
        exit 1
    fi
    export MAVEN_URL="https://apache.osuosl.org/maven/maven-3/$MAVEN_VERSION/binaries/apache-maven-$MAVEN_VERSION-bin.tar.gz"
    echo "Maven File URL: [${MAVEN_URL}]"
}


get_liberica_jdk_fetch_url() {
    unset JDK_FETCH_URL    
    OS=linux 
    IMAGE_TYPE="jdk"
    DIST="tar.gz"

    if [[ $ARCH == "arm64" ]]; then
        export ARCH="arm"
    fi
    
    API_RESPONSE=$(curl -s "https://api.bell-sw.com/v1/liberica/releases?version-modifier=latest&bitness=64&os=${OS}&arch=${ARCH}&package-type=${DIST}&bundle-type=${IMAGE_TYPE}")
    echo $API_RESPONSE

    FEATURE_VERSION=$(echo ${API_RESPONSE} | jq -r '.[0].version')    
    echo "JDK version [${FEATURE_VERSION}]"
    
    export JDK_FETCH_URL=$(echo ${API_RESPONSE} | jq -r '.[0].downloadUrl')
    echo "JDK file fetch url: [${JDK_FETCH_URL}]"
}

get_liberica_jre_fetch_url() {
    unset JRE_FETCH_URL

    OS=linux 
    IMAGE_TYPE="jre" 
    DIST="tar.gz"

    if [[ $ARCH == "arm64" ]]; then
        export ARCH="arm"
    fi
    
    API_RESPONSE=$(curl -s "https://api.bell-sw.com/v1/liberica/releases?version-modifier=latest&bitness=64&os=${OS}&arch=${ARCH}&package-type=${DIST}&bundle-type=${IMAGE_TYPE}")
    echo $API_RESPONSE

    FEATURE_VERSION=$(echo ${API_RESPONSE} | jq -r '.[0].version')    
    echo "JRE version [${FEATURE_VERSION}]"
    
    export JRE_FETCH_URL=$(echo ${API_RESPONSE} | jq -r '.[0].downloadUrl')
    echo "JRE file fetch url: [${JRE_FETCH_URL}]"
}

get_eclipse_jdk_fetch_url() {
    unset JDK_FETCH_URL
    FEATURE_VERSION=$(curl -s https://api.adoptium.net/v3/info/available_releases | jq '.most_recent_feature_release')

    echo "JDK version [${FEATURE_VERSION}]"

    OS=linux
    IMAGE_TYPE=jdk

    if [[ $ARCH == "arm64" ]]; then
        export ARCH="aarch64"
    fi

    export JDK_FETCH_URL="https://api.adoptium.net/v3/binary/latest/${FEATURE_VERSION}/ga/${OS}/${ARCH}/${IMAGE_TYPE}/hotspot/normal/eclipse"
    echo "JDK file fetch url: [${JDK_FETCH_URL}]"
}

get_eclipse_jre_fetch_url() {
    unset JRE_FETCH_URL

    FEATURE_VERSION=$(curl -s https://api.adoptium.net/v3/info/available_releases | jq '.most_recent_feature_release')

    echo "JRE version [${FEATURE_VERSION}]"

    OS=linux
    IMAGE_TYPE=jre

    if [[ $ARCH == "arm64" ]]; then
        export ARCH="aarch64"
    fi

    export JRE_FETCH_URL="https://api.adoptium.net/v3/binary/latest/${FEATURE_VERSION}/ga/${OS}/${ARCH}/${IMAGE_TYPE}/hotspot/normal/eclipse"
    echo "JRE file fetch url: [${JRE_FETCH_URL}]"
}

get_jdk_fetch_url() {
    export JDK_DIST=${JDK_DIST:-${JAVA_DIST}}
    echo "Using JDK distribution [${JDK_DIST}]"
    if [[ "$JDK_DIST" = "eclipse" ]]; then
        get_eclipse_jdk_fetch_url
    elif [[ "$JDK_DIST" = "liberica" ]]; then
        get_liberica_jdk_fetch_url
    else
        >&2 echo "Unknown JDK spec. Cannot continue."
        exit 1
    fi
}

get_jre_fetch_url() {
    export JRE_DIST=${JRE_DIST:-${JAVA_DIST}}
    echo "Using JRE distribution [${JRE_DIST}]"
    if [[ "$JRE_DIST" = "eclipse" ]]; then
        get_eclipse_jre_fetch_url
    elif [[ "$JRE_DIST" = "liberica" ]]; then
        get_liberica_jre_fetch_url
    else
        >&2 echo "Unknown JDK spec. Cannot continue."
        exit 1
    fi
}


get_arch

JAVA_DIST=${JAVA_DIST:-'liberica'}

get_jre_fetch_url
if [[ -z $JRE_FETCH_URL ]]; then
    >&2 echo "Invalid fetch url for JRE. Cannot build."
    exit 1
fi

get_jdk_fetch_url
if [[ -z $JDK_FETCH_URL ]]; then
    >&2 echo "Invalid fetch url for JDK. Cannot build."
    exit 1
fi

get_maven_url
if [[ -z $MAVEN_URL ]]; then
    >&2 echo "Invalid fetch url for maven. Cannot build."
    exit 1
fi

# Generate timestamp
TIMESTAMP=$(date +"%Y-%m-%d %H:%M:%S")
JAVA_URL_ENV_FILE="java_urls.env"

cat << EOF > ${JAVA_URL_ENV_FILE}
# file generated at timestamp ${TIMESTAMP}

# JRE for prod
JRE_DIST=${JRE_DIST}
JRE_FETCH_URL=${JRE_FETCH_URL}

# JDK for dev, and build
JDK_DIST=${JDK_DIST}
JDK_FETCH_URL=${JDK_FETCH_URL}

# maven for vscode dev environment, and sample project creation
# rest of the project uses mvn-wrapper
MAVEN_VERSION=${MAVEN_VERSION}
MAVEN_URL=${MAVEN_URL}
EOF

if git status --porcelain "$JAVA_URL_ENV_FILE" | grep -q "^ M"; then
    >&2 echo "Java tool versions changed. Review the env file [${JAVA_URL_ENV_FILE}]"
    exit 1
fi
