# Update the java version to use

## change the release to use
The variable `JAVA_DIST` will change the release of java to use for the build.
The default value for this variable is set to `liberica`. The alternative is `eclipse`.

To update it, there are two ways:
1. Change the value in the `fetch-java-versions.sh` file
2. Set the value of that variable when running the `fetch-java-versions.sh` file

## Update Java release to use
The java release used in the build will be taken from the java_urls.env file.
To update the file with the latest version, run the `fetch-java-versions.sh` file, as mentioned above.

If there is a new release of java detected, and hence the env file changes, the script exits with an error. 
At that point, please commit the file after reviewing the change.

# Setup devcontainer upstream

Set up a readonly remote upstream to the dev container repo:
```bash
git remote add devcon-upstream git@gitlab.com:mandraketech/java-vscode-dev.git
git remote set-url --push devcon-upstream no_push
```

Use the following command to pull the upstream:
```bash
git fetch devcon-upstream
```

And then follow the regular rebase/merge workflows:
```bash
git merge devcon-upstream/main
```