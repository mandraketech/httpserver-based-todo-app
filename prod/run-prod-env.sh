RUN_ENV=prod

ENV_FILE=`realpath .env`

if [[ ! -f ${ENV_FILE} ]]; then
    >&2 echo "Environment not defined. Cannot find file [${ENV_FILE}]. Stopping.".
    exit 1
fi

# get the app name from the env, and export it, for later use
source ${ENV_FILE}
if [[ -z ${COMPOSE_PROFILES} ]]; then
  if [[ ! -z $TUNNEL_TOKEN ]]; then
    export COMPOSE_PROFILES=tunnel
  else
    export COMPOSE_PROFILES=loadbalancer
  fi
fi

# process the command
RUN_CMD=$1
DOCKER_COMPOSE_FILE_PROD="./prod/docker-compose.prod.yml"

case ${RUN_CMD} in
    "start")
        # do not need token in case of any operation other than start
        docker compose -f ${DOCKER_COMPOSE_FILE_PROD} up -d
        docker compose -f ${DOCKER_COMPOSE_FILE_PROD} logs --follow app reverse-tunnel loadbalancer
        ;;
    "stop") 
        docker compose -f ${DOCKER_COMPOSE_FILE_PROD} down -v
        ;;
    "exec")
        # consume the exec command
        shift
        if [[ $# -lt 2 ]]; then
            >&2 echo "Need container name and command. eg. 'app bash'"
            exit 1
        fi
        docker compose -f ${DOCKER_COMPOSE_FILE_PROD} exec $@
        ;;
    *)
        echo "Pass through all args: [$@]"
        docker compose -f ${DOCKER_COMPOSE_FILE_PROD} $@
        ;;
esac