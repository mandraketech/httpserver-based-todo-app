package com.example;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.Base64;

import org.junit.jupiter.api.AfterAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import com.example.config.LoggerConfig;
import com.example.router.RouteResponse;
import com.example.router.RouteResponse.ErrorResponses;

class AppEndpointIT {

  interface StopHandler {
    void stop();
  }

  static StopHandler stopHandler;
  static String baseAddress;

  @BeforeAll
  public static void initializeApp() throws IOException {
    LoggerConfig.setupGlobalConsoleLogger();
    var PORT = 8080;
    var app = new App(PORT);
    baseAddress = app.start();
    stopHandler = () -> app.stop(0);
  }

  @AfterAll
  public static void stopApp() {
    stopHandler.stop();
  }

  @Test
  void testExistingGetHandler() throws IOException, InterruptedException {
    java.net.http.HttpResponse<String> response;
    try (var client = HttpClient.newHttpClient()) {
      var uri = URI.create("%s/%s".formatted(baseAddress, "api/ping"));
      response = client.send(
          HttpRequest.newBuilder().GET().uri(uri).build(),
          BodyHandlers.ofString());
    }

    assertNotNull(response, "Response is null");
    assertEquals(200, response.statusCode());
    assertEquals("pong", response.body());
  }

  @Test
  void testExistingPostHandler() throws IOException, InterruptedException {
    java.net.http.HttpResponse<String> response;
    try (var client = HttpClient.newHttpClient()) {
      var uri = URI.create("%s/%s".formatted(baseAddress, "api/ping"));
      response = client.send(
          HttpRequest.newBuilder().POST(HttpRequest.BodyPublishers.ofString("dummy body content")).uri(uri).build(),
          BodyHandlers.ofString());
    }

    assertEquals(ErrorResponses.UNKNOWN_ENDPOINT_RESPONSE.toValue(), extractAsRouteResponse(response));
  }

  @Test
  void testNonExistentEndpointHandler() throws IOException, InterruptedException {
    java.net.http.HttpResponse<String> response;
    try (var client = HttpClient.newHttpClient()) {
      var uri = URI.create("%s/%s".formatted(baseAddress, "pingping"));
      response = client.send(
          HttpRequest.newBuilder().GET().uri(uri).build(),
          BodyHandlers.ofString());
    }

    assertEquals(ErrorResponses.UNKNOWN_ENDPOINT_RESPONSE.toValue(), extractAsRouteResponse(response));
  }

  @Test
  void test404Handler() throws IOException, InterruptedException {
    java.net.http.HttpResponse<String> response;
    try (var client = HttpClient.newHttpClient()) {
      var uri = URI.create("%s/%s".formatted(baseAddress, "404"));
      response = client.send(
          HttpRequest.newBuilder().GET().uri(uri).build(),
          BodyHandlers.ofString());
    }

    assertEquals(ErrorResponses.UNKNOWN_ENDPOINT_RESPONSE.toValue(), extractAsRouteResponse(response));
  }

  @Test
  void testExistingIndexHtmlHandler() throws IOException, InterruptedException {
    java.net.http.HttpResponse<String> response;
    try (var client = HttpClient.newHttpClient()) {
      var uri = URI.create("%s/".formatted(baseAddress));
      response = client.send(
          HttpRequest.newBuilder().GET().uri(uri).build(),
          BodyHandlers.ofString());
    }

    assertNotNull(response, "Response is null");
    assertEquals(200, response.statusCode());
    assertNotNull(response.body());
    assertFalse(response.body().isBlank());
  }

  @ParameterizedTest
  @ValueSource(strings = {"admin:admin", "user:user"})
  void testSecureUserHtmlHandler(String creds) throws IOException, InterruptedException {
    java.net.http.HttpResponse<String> response;
    var username = creds.split(":")[0];
    try (var client = HttpClient.newHttpClient()) {
      var uri = URI.create("%s/api/user/me".formatted(baseAddress));

      // get base64 version of valid creds
      var encodedCreds = Base64.getEncoder().encodeToString(creds.getBytes());

      response = client.send(
          HttpRequest.newBuilder().GET().uri(uri).header("Authorization", "Basic %s".formatted(encodedCreds)).build(),
          BodyHandlers.ofString());
    }

    assertNotNull(response, "Response is null");
    assertEquals(200, response.statusCode());
    assertNotNull(response.body());
    assertEquals(String.format("<html><body>Hello %s</body></html>", username), response.body());
  }

  @ParameterizedTest
  @ValueSource(strings = {"admin:someone", "admin:", ""})
  void testSecureUser401Handler(String creds) throws IOException, InterruptedException {
    java.net.http.HttpResponse<String> response;
    try (var client = HttpClient.newHttpClient()) {
      var uri = URI.create("%s/api/user/random".formatted(baseAddress));
      // get base64 version of invalid creds
      var encodedCreds = Base64.getEncoder().encodeToString(creds.getBytes());

      response = client.send(
          HttpRequest.newBuilder().GET().uri(uri).header("Authorization", "Basic %s".formatted(encodedCreds)).build(),
          BodyHandlers.ofString());
    }

    assertEquals(ErrorResponses.UNAUTHORIZED_ENDPOINT_RESPONSE.toValue(), extractAsRouteResponse(response));
  }

  @Test
  void testSecureAdminHtmlHandler() throws IOException, InterruptedException {
    java.net.http.HttpResponse<String> response;
    try (var client = HttpClient.newHttpClient()) {
      var uri = URI.create("%s/api/admin".formatted(baseAddress));

      // get base64 version of valid creds
      var creds = Base64.getEncoder().encodeToString("admin:admin".getBytes());

      response = client.send(
          HttpRequest.newBuilder().GET().uri(uri).header("Authorization", "Basic %s".formatted(creds)).build(),
          BodyHandlers.ofString());
    }

    assertNotNull(response, "Response is null");
    assertEquals(200, response.statusCode());
    assertNotNull(response.body());
    assertEquals("<html><body>Hello Admin admin</body></html>", response.body());
  }

  @ParameterizedTest
  @ValueSource(strings = {"admin:someone", "admin:", ""})
  void testSecureAdmin401Handler(String creds) throws IOException, InterruptedException {
    java.net.http.HttpResponse<String> response;
    try (var client = HttpClient.newHttpClient()) {
      var uri = URI.create("%s/api/admin".formatted(baseAddress));
      // get base64 version of invalid creds
      var encodedCreds = Base64.getEncoder().encodeToString(creds.getBytes());

      response = client.send(
          HttpRequest.newBuilder().GET().uri(uri).header("Authorization", "Basic %s".formatted(encodedCreds)).build(),
          BodyHandlers.ofString());
    }

    assertEquals(ErrorResponses.UNAUTHORIZED_ENDPOINT_RESPONSE.toValue(), extractAsRouteResponse(response));
  }


  private RouteResponse extractAsRouteResponse(HttpResponse<String> response) {
    var contentType = response.headers().firstValue("Content-Tyoe").orElse(null);
    var body = response.body();
    var resp = RouteResponse.withStatus(response.statusCode())
        .withHeaders(response.headers().map());
    if (body != null && !body.isBlank()) {
      resp = resp.withBody(contentType, body);
    }
    return resp;
  }

}
