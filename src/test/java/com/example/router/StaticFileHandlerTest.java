package com.example.router;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.net.URI;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import com.example.config.LoggerConfig;
import com.example.endpoints.StaticFileHandler;

public class StaticFileHandlerTest {

  private static StaticFileHandler handler;

  @BeforeAll
  public static void setup() {
    LoggerConfig.setupGlobalConsoleLogger();
    handler = new StaticFileHandler("src/main/resources", true); 
  }

  @ParameterizedTest
  @ValueSource(strings = {"/","/index.html"})
  void testHandleGetExistingHtmlFile(String uriPath) {
    RouteRequest req = new RouteRequest(RequestMethod.GET, URI.create(uriPath));
    RouteResponse resp = handler.handle(req);

    assertEquals(200, resp.statusCode());
    assertEquals(List.of("text/html"), resp.headers().get("Content-Type"));
    assertNotNull(resp.body());
  }

  @Test
  void testHandleGetExistingCssFile() {
    RouteRequest req = new RouteRequest(RequestMethod.GET, URI.create("/styles.css"));
    RouteResponse resp = handler.handle(req);

    assertEquals(200, resp.statusCode());
    assertEquals(List.of("text/css"), resp.headers().get("Content-Type"));
    assertNotNull(resp.body());
  }

  @Test 
  void testHandleGetNonExistentFile() {
    RouteRequest req = new RouteRequest(RequestMethod.GET, URI.create("/fake.txt"));
    RouteResponse resp = handler.handle(req);

    assertNull(resp);
  }

  @Test 
  void testHandleNoRootResolve() {
    var handler = new StaticFileHandler("src/main/resources", false); 
    RouteRequest req = new RouteRequest(RequestMethod.GET, URI.create("/"));
    RouteResponse resp = handler.handle(req);

    assertNull(resp);
  }

  @Test
  void testHandlePost() {
    RouteRequest req = new RouteRequest(RequestMethod.POST, URI.create("/index.html"));
    RouteResponse resp = handler.handle(req);

    assertNull(resp);
  }
}
