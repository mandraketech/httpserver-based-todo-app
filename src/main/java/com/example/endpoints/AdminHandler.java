package com.example.endpoints;

import com.example.router.RequestMethod;
import com.example.router.RouteRequest;
import com.example.router.RouteResponse;

public interface AdminHandler {
  
  static RouteResponse handle(RouteRequest req) {
    if (!req.hasUserRole("ADMIN")) {
      return RouteResponse.ErrorResponses.UNAUTHORIZED_ENDPOINT_RESPONSE.toValue();
    }

    if (req.method() == RequestMethod.GET) {
      return RouteResponse.withStatus(200).withHtmlBody("<html><body>Hello Admin %s</body></html>".formatted(req.principal().getUsername()));
    }
    return null;
  }
}
