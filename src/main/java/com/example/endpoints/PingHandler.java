package com.example.endpoints;

import com.example.router.RequestMethod;
import com.example.router.RouteRequest;
import com.example.router.RouteResponse;

public interface PingHandler {
  
  static RouteResponse handle(RouteRequest req) {
    if (req.method() == RequestMethod.GET) {
      return RouteResponse.withStatus(200).withTextBody("pong");
    }
    return null;
  }
}
