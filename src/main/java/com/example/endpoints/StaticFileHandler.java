package com.example.endpoints;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

import com.example.config.LoggerConfig;
import com.example.router.RequestMethod;
import com.example.router.RouteRequest;
import com.example.router.RouteResponse;

/**
 * a static file handler that will serve content from the specified folder
 * 
 * @author vscode
 */
public class StaticFileHandler {
  private final String folder;
  private final String rootPathFile;

  /**
   * 
   * @param folder          folder for the static files
   * @param resolveRootPath if true, the root url "/" will be resolved to one of
   *                        [index.html, index.htm]
   */
  public StaticFileHandler(String folder, boolean resolveRootPath) {
    // ensure folder exists, otherwise throw a runtime exception
    if (!Files.isDirectory(Paths.get(folder))) {
      throw new IllegalArgumentException("folder does not exist: " + folder);
    }

    this.folder = folder;

    // check for existence of one of [index.html, index.htm]
    // save that as the rootPathFile
    rootPathFile = resolveRootPath ? resolveIndexFileName(folder) : null;
  }

  private String resolveIndexFileName(String folder) {
    // check for existence of one of [index.html, index.htm]
    String[] rootFileList = { "index.html", "index.htm" };
    for (var file : rootFileList) {
      if (Files.exists(Paths.get(folder, file))) {
        return "/" + file;
      }
    }

    throw new IllegalStateException(
        "No index file, from list [%s], found in folder: %s ".formatted(rootFileList, folder));
  }

  private String computeUriPath(final String uriPath) {
    // never allow access to root folder
    if (rootPathFile != null && uriPath.equals("/")) {
      // resolve the root path to the root file
      return rootPathFile;
    } else {
      // 404: Not Found is automatically handled
      return uriPath;
    }
  }

  public RouteResponse handle(RouteRequest req) {
    // only support get calls
    if (req.method() != RequestMethod.GET) {
      // 404: Not Found is automatically handled
      return null;
    }

    String uriPath = computeUriPath(req.uri().getPath());
    // never allow access to any directory
    var path = Paths.get(this.folder, uriPath.isBlank() ? rootPathFile : uriPath);

    // if the file doesn't exist, or is a directory, return null
    if (!Files.isRegularFile(path)) {
      // 404: Not Found is automatically handled
      return null;
    }

    // get content mime time based on file name
    try {
      var contentType = Files.probeContentType(path);
      if (contentType == null) {
        throw new IllegalArgumentException("Unable to determine content type for file: " + path);
      }
      // read the file and return in the response
      var fileContent = new String(Files.readAllBytes(path));
      return RouteResponse.withStatus(200).withBody(contentType, fileContent);
    } catch (IOException e) {
      LoggerConfig.getApplicationLogger().severe(() -> String.format("Error while accessing file: %s ", path));
      LoggerConfig.getApplicationLogger().severe(() -> String.format("Error Message: %s ", e.getLocalizedMessage()));
      LoggerConfig.getApplicationLogger().severe(() -> String.format("Error Trace: %s ", Arrays.toString(e.getStackTrace())) );
      
      return RouteResponse.withStatus(403).withTextBody("Forbidden");
    }
  }
}