package com.example.endpoints;

import com.example.router.RequestMethod;
import com.example.router.RouteRequest;
import com.example.router.RouteResponse;

public interface UserHandler {
  
  static RouteResponse handle(RouteRequest req) {
    if (!req.hasUserRole("USER")) {
      return RouteResponse.ErrorResponses.UNAUTHORIZED_ENDPOINT_RESPONSE.toValue();
    }

    if (req.method() == RequestMethod.GET && req.uri().getPath().equals("me")) {
      return RouteResponse.withStatus(200).withHtmlBody("<html><body>Hello %s</body></html>".formatted(req.principal().getUsername()));
    }
    return null;
  }
}
