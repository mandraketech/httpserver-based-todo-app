package com.example.auth;

import java.util.List;
import java.util.Map;

public final class UserRepository {
  final Map<String, String> userCredMap;
  final Map<String, List<String>> userRolesMap;
  
  @SuppressWarnings("java:S1192")
  public UserRepository() {
    userCredMap = Map.of("admin", "admin", "user", "user");
    userRolesMap = Map.of("admin", List.of("ADMIN", "USER"), "user", List.of("USER"));
  }

  /**
   * checks for credential validity
   * @param username
   * @param password
   * @return
   */
  public boolean checkCredentials(String username, String password) {
    return userCredMap.containsKey(username) && userCredMap.get(username).equals(password);
  }

  /**
   * Assumes that the checkCredentials has been called earlier
   * @param username
   * @return
   */
  public List<String> fetchRoleList(String username) {
    return userRolesMap.get(username);
  }
}
