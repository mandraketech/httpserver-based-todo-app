package com.example.auth;

import java.util.List;
import java.util.Objects;

import com.example.router.authenticator.UserRoleAuthenticator;
import com.sun.net.httpserver.BasicAuthenticator;

public interface AuthenticatorFactory {

  public static UserRoleAuthenticator createBasicAuthenticator(UserRepository repository) {
    Objects.requireNonNull(repository, "If UserRepository is null, authentication cannot work");

    var basicAuth = new BasicAuthenticator("sample") {
      @Override
      public boolean checkCredentials(String username, String password) {
        return repository.checkCredentials(username, password);
      }
    };

    return new UserRoleAuthenticator(basicAuth) {    
      @Override
      public List<String> getUserRoles(String username) {
        return repository.fetchRoleList(username);
      }
    };
  }
}

