package com.example.config;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.ConsoleHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public interface LoggerConfig {
  
  public static final String SUB_LOGGER_FORMAT = "%s.%s";

  static void setupGlobalConsoleLogger() {
    // Get the global logger
    Logger globalLogger = Logger.getLogger("");
    // always log info and above for all loggers
    globalLogger.setLevel(Level.INFO);

    // Remove any existing handlers to avoid duplication
    Handler[] handlers = globalLogger.getHandlers();
    for (Handler handler : handlers) {
      globalLogger.removeHandler(handler);
    }

    // Create a console handler and set its log level
    var consoleHandler = new ConsoleHandler();

    consoleHandler.setFilter(f -> true);
    
    consoleHandler.setFormatter(new Formatter() {
      final SimpleDateFormat loggerDateFormat = new SimpleDateFormat("dd/MMM/yy HH:mm:ss.SSS");
      public String format(LogRecord lr) {
        var message = lr.getMessage();
        var params = lr.getParameters();

        if (params != null && params.length > 0) {
          message = MessageFormat.format(message, params);
        }

        String formattedDate = loggerDateFormat.format(Date.from(lr.getInstant()));

        return String.format("%d::%s::%s::%s::%s::%s%n",
                lr.getLongThreadID(), lr.getLoggerName(), lr.getSourceClassName(), lr.getSourceMethodName(), formattedDate, message);
      }
    });
    // Add the console handler to the global logger
    globalLogger.addHandler(consoleHandler);

    // setup the specific app loggers after the global settings
    if (RuntimeConfig.getInstance().runMode() == RuntimeConfig.RunMode.PROD) {
      // only log sever message from the debug logs, in prod
      getDebugLogger().setLevel(Level.SEVERE);
    } else {
      getDebugLogger().setLevel(Level.FINE);
    }
  }

  static String APP_LOGGER_NAME = "app";

  static Logger getApplicationLogger() {
    return Logger.getLogger(APP_LOGGER_NAME);
  }

  static Logger getApplicationLogger(String subLogger) {
    return Logger.getLogger(String.format(subLogger, APP_LOGGER_NAME, subLogger));
  }

  static String DEBUG_LOGGER_NAME = "debug";

  static Logger getDebugLogger() {
    return Logger.getLogger(DEBUG_LOGGER_NAME);
  }

  static Logger getDebugLogger(String subLogger) {
    return Logger.getLogger(String.format(SUB_LOGGER_FORMAT, DEBUG_LOGGER_NAME, subLogger));
  }
}
