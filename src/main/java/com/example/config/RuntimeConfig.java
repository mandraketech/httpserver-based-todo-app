package com.example.config;

/**
 * Singleton for "Runtime" environment based computed state
 */
@SuppressWarnings("java:S6548")
public final class RuntimeConfig {
  
  public enum RunMode {DEV, TEST, PROD}
  private final RunMode runMode;

  private static class Holder {
    private static final RuntimeConfig INSTANCE = new RuntimeConfig();
  }

  public static final RuntimeConfig getInstance() {
    return Holder.INSTANCE;
  }

  public RunMode runMode() { return runMode; }

  private RuntimeConfig() {
    // if the Env variable PROD is set, then runmode is PROD

    this.runMode = switch (System.getProperty("RUN_ENV", "DEV")) {
      case "PROD" -> RunMode.PROD;
      case "TEST" -> RunMode.TEST;
      default -> RunMode.DEV;
    };
  }
}
