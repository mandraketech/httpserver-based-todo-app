package com.example.router;

import java.util.List;
import java.util.Map;

import com.sun.net.httpserver.Headers;

/**
 * A response to a request.
 * @param statusCode the status code of the response
 * @param headers the headers of the response
 * @param body the body of the response
 */
public class RouteResponse {

    public enum ErrorResponses {
        ERROR_ENDPOINT_RESPONSE(RouteResponse.withStatus(500)),
        UNKNOWN_ENDPOINT_RESPONSE(RouteResponse.withStatus(404)),
        UNAUTHORIZED_ENDPOINT_RESPONSE(RouteResponse.withStatus(401));

        final RouteResponse response;

        ErrorResponses(RouteResponse response) {
            this.response = response;
        }

        public RouteResponse toValue() {
            return this.response;
        }
    }


    // Plain Text
    public static final String CONTENT_TYPE_TEXT = "text/plain";
    // JSON
    public static final String CONTENT_TYPE_JSON = "application/json";
    // HTML
    public static final String CONTENT_TYPE_HTML = "text/html";


    private int statusCode;
    private Map<String, List<String>> headers = new Headers();
    private String body;

    private static final String CONTENT_TYPE = "Content-Type";

    private RouteResponse() {}

    public static RouteResponse withStatus(int statusCode) {
        // ensure valid status
        if (statusCode < 100 || statusCode >= 600) {
            throw new IllegalArgumentException("Status code must be between 100 and 599");
        }

        var retVal = new RouteResponse();
        retVal.statusCode = statusCode;
        return retVal;
    }

    public RouteResponse withBody(String contentType, String body) {
        if ( contentType == null || body == null) {
            throw new IllegalArgumentException("contentType and body must not be null");
        }
        headers.put(CONTENT_TYPE, List.of(contentType));
        this.body = body;
        
        return this;
    }

    public RouteResponse withTextBody(String body) {
        headers.put(CONTENT_TYPE, List.of(CONTENT_TYPE_TEXT));
        this.body = body;
        
        return this;
    }

    public RouteResponse withJsonBody(String body) {
        headers.put(CONTENT_TYPE, List.of(CONTENT_TYPE_JSON));
        this.body = body;
        
        return this;
    }

    public RouteResponse withHtmlBody(String body) {
        headers.put(CONTENT_TYPE, List.of(CONTENT_TYPE_HTML));
        this.body = body;
        
        return this;
    }


    public RouteResponse withHeader(String key, String value) {
        if (key == null || value == null) {
            throw new IllegalArgumentException("key and value must not be null");
        }
        headers.put(key, List.of(value));
        return this;
    }

    public RouteResponse withHeaders(Map<String, List<String>> headers) {
        if (headers == null) {
            throw new IllegalArgumentException("headers must not be null");
        }
        
        this.headers.putAll(headers);
        return this;
    }

    public int statusCode() {
        return statusCode;
    }

    public Map<String, List<String>> headers() {
        return headers;
    }

    public String contentType() {
        return headers.get(CONTENT_TYPE) != null ? headers.get(CONTENT_TYPE).getFirst() : null;
    }

    public String body() {
        return body;
    }

    public boolean hasContentType() {
        return headers.get(CONTENT_TYPE) != null;
    }

    public boolean validateContentType() {
        // don't care about the content type for non-success conditions
        if (statusCode < 200 || statusCode >= 300) {
            return true;
        }
        if (body == null) {
            return true;
        } else {
            return hasContentType();
        }
    }

    @Override
    public String toString() {
        return """
            RouteResponse{
                  statusCode=%s
                  , headers=%s
                  , body='%s"'
                }
        """.formatted(statusCode, headers, body);
    }

    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RouteResponse that = (RouteResponse) o;

        if (statusCode != that.statusCode) return false;
        // ensure current headers is a subset of "that" headers
        if(!that.headers.keySet().containsAll(headers.keySet())) return false;
        var diff = headers.keySet().stream().filter(key -> !that.headers.get(key).equals(headers.get(key))).toList();
        if (!diff.isEmpty()) return false;
        return body != null ? body.equals(that.body) : that.body == null;
    }

    @Override
    public int hashCode() {
        int result = statusCode;
        result = 31 * result + headers.hashCode();
        result = 31 * result + (body != null ? body.hashCode() : 0);
        return result;
    }
}