package com.example.router;

import java.util.TreeSet;
import java.util.function.Function;

import com.sun.net.httpserver.Authenticator;
import com.sun.net.httpserver.HttpServer;

public interface RouteBuilder {
  interface RouteManager {

    /**
     * Adds a handler against the given path and get500InternalErrorResponse.
     *
     * @param path    path for the handler
     * @param handler the handler that will take a request, and return a
     *                response
     * @return a reference to the builder, so that a fluent style of
     *         programming can be used
     * @throws IllegalArgumentException if a handler is already registered
     *                                  for the given path
     */
    RouteManager createContext(String path, Function<RouteRequest, RouteResponse> handler);
    RouteManager setAuthenticator(Authenticator authenticator);
  }

  public static RouteManager withServer(final HttpServer server) {

    return new RouteManager() {
      private TreeSet<String> routes = new TreeSet<>();
      private Authenticator authenticator;

      @Override
      public RouteManager createContext(String path, Function<RouteRequest, RouteResponse> handler) {
        if (routes.contains(path)) {
          throw new IllegalArgumentException("Endpoint already registered: %s".formatted(path));
        }
        var context = server.createContext(path, new WrappedRouter(path, handler));

        if (authenticator != null) {
          context.setAuthenticator(authenticator);
        }

        return this;
      }

      @Override
      public RouteManager setAuthenticator(Authenticator a) {
        this.authenticator = a;
        return this;
      }
    };
  }
}