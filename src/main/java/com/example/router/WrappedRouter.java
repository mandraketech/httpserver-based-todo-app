package com.example.router;

import java.io.IOException;
import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import com.example.config.LoggerConfig;
import com.example.router.RouteResponse.ErrorResponses;
import com.example.router.authenticator.UserRolePrincipal;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpPrincipal;


public class WrappedRouter implements HttpHandler {

    private final String basePath;
    private final Function<RouteRequest, RouteResponse> handler;

    public WrappedRouter(String basePath, Function<RouteRequest, RouteResponse> handler) {
        Objects.requireNonNull(handler, "Cannot wrap a 'null' handler");
        Objects.requireNonNull(basePath, "cannot have a null base path");
        
        this.basePath = basePath;
        this.handler = handler;
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        var req = createRouteRequest(basePath, exchange);
        var response = processRequest(req);
        // send the response, and clean up
        sendResponse(exchange, response);
    }

    RouteResponse processRequest(RouteRequest req) {
        LoggerConfig.getDebugLogger().info(() -> String.format("Processing request: %s", req.uri().getPath()));
        RouteResponse response = null;

        try {                
            response = handler.apply(req);
        } catch (RuntimeException e) {
            response = handleInternalError(req, e);
        }

        // if no handling has been done, then its a 404 !!
        if (response == null) {
            LoggerConfig.getDebugLogger().info(() -> String.format("Response is null on request for: %s. Returning 404.", req.uri().getPath()));
            response = ErrorResponses.UNKNOWN_ENDPOINT_RESPONSE.toValue();
        }

        return response;
    }

    private RouteResponse handleInternalError(RouteRequest req, RuntimeException e) {
        // log exception
        LoggerConfig.getApplicationLogger().severe( () -> String.format("Error while processing request: %s", req.redacted()));
        LoggerConfig.getApplicationLogger().severe( () -> String.format("Source exception message: %s", e.getLocalizedMessage()));
        LoggerConfig.getApplicationLogger().severe( () -> String.format("Source exception trace: %s", Arrays.toString(e.getStackTrace())));

        return ErrorResponses.ERROR_ENDPOINT_RESPONSE.toValue();
    }

    private void sendResponse(HttpExchange exchange, RouteResponse response) throws IOException {
        if (!response.validateContentType()) {
            throw new IllegalArgumentException("Invalid response object. Need content type: %s".formatted(response));
        }

        try (var os = exchange.getResponseBody()) {
            int respBodyLen = response.body() == null ? -1 : response.body().getBytes().length;
            LoggerConfig.getDebugLogger().info( () -> String.format("Response body length: %d", respBodyLen));
            
            exchange.sendResponseHeaders(response.statusCode(), respBodyLen);
            var responseHeaders = exchange.getResponseHeaders();
            // add the headers from the response to the exchange
            responseHeaders.putAll(response.headers());
            if (respBodyLen >= 0) {
                os.write(response.body().getBytes());
            }
        }
    }

    private RouteRequest createRouteRequest(String basePath, HttpExchange exchange) {
        var principal = switch (exchange.getPrincipal()) {
            case null -> null;
            case UserRolePrincipal up -> up;
            case HttpPrincipal p -> new UserRolePrincipal(p.getName(), p.getRealm(), List.of());
        };

        // ensure headers "keys" are all lower case
        var uri = exchange.getRequestURI().getPath();
        if ( !uri.startsWith(basePath) ) {
            LoggerConfig.getApplicationLogger().warning(() -> String.format("URI(%s) does not start with %s",uri, basePath));
        }
        // remove the basePath prefix from the uri
        var trimUri = uri.substring(basePath.length());

        return new RouteRequest(
                // ensure headers in teh request are readonly
                Headers.of(exchange.getRequestHeaders()),
                URI.create(trimUri),
                RequestMethod.valueOf(exchange.getRequestMethod()),
                exchange.getRequestBody(),
                exchange.getRemoteAddress(),
                exchange.getLocalAddress(),
                exchange.getProtocol(),
                principal
        );
    }
}
