package com.example.router;

import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.URI;
import java.util.List;
import java.util.Map;

import com.example.router.authenticator.UserRolePrincipal;
import com.sun.net.httpserver.Headers;

public record RouteRequest(
        Map<String, List<String>> headers,
        URI uri,
        RequestMethod method,
        InputStream body,
        InetSocketAddress remoteAddress,
        InetSocketAddress localAddress,
        String protocol,
        UserRolePrincipal principal) {

    public RouteRequest {
        // ensure headers are readonly in request
        headers = Headers.of(headers);
    }

    public RouteRequest(RequestMethod method, URI uri) {
        this(Headers.of(), uri, method, null, null, null, null, null);
    }

    public String redacted() {
        return "RouteRequest [uri=%s, method=%s]".formatted(uri.getPath(), method.name());
    }

    public boolean hasUserRole(String role) {
        return principal != null && principal.getRoles().contains(role);
    }
}