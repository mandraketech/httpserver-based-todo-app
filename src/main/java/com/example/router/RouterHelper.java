package com.example.router;

import java.net.URLConnection;

interface RouterHelper {
  
  public static String getMimeTypeForFileExt(String ext) {
    return URLConnection.getFileNameMap().getContentTypeFor("file:///index.%s".formatted(ext));
  }
}
