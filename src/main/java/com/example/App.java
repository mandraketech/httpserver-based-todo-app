package com.example;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.text.DateFormat;
import java.util.Date;
import com.example.auth.AuthenticatorFactory;
import com.example.auth.UserRepository;
import com.example.config.LoggerConfig;
import com.example.config.RuntimeConfig;
import com.example.endpoints.AdminHandler;
import com.example.endpoints.PingHandler;
import com.example.endpoints.StaticFileHandler;
import com.example.endpoints.UserHandler;
import com.example.router.RouteResponse;
import com.example.router.RouteBuilder;
import com.sun.net.httpserver.HttpServer;

/**
 * Hello world!
 *
 */
public class App {
    private static final String CONTENT_TYPE_TEXT = null;
    private final HttpServer server;
    
    public App(int httpPort) throws IOException {
        // If running in prod, listen to all connections, even from external
        // in all other cases, only listen to localhost, so only testing !!
        if (RuntimeConfig.getInstance().runMode() == RuntimeConfig.RunMode.PROD) {
            server = HttpServer.create(new InetSocketAddress("0.0.0.0", httpPort), 0);
        } else {
            server = HttpServer.create(new InetSocketAddress("127.0.0.1", httpPort), 0);
        }

        var userRepository = new UserRepository();
        var userRoleBasicAuthenticator = AuthenticatorFactory.createBasicAuthenticator(userRepository);

        RouteBuilder.withServer(server)
            .createContext("/", new StaticFileHandler("src/main/resources", true)::handle);
            
        RouteBuilder.withServer(server)
            .createContext("/api/ping", PingHandler::handle)
            .createContext("/api/time", req -> RouteResponse.withStatus(200).withBody(CONTENT_TYPE_TEXT, DateFormat.getDateTimeInstance().format(new Date())));

        RouteBuilder.withServer(server).setAuthenticator(userRoleBasicAuthenticator)
            .createContext("/api/user/", UserHandler::handle)
            .createContext("/api/admin", AdminHandler::handle);
            
    }

    public String start() {
        this.server.start();
        var baseUrl = "http://%s:%d".formatted(server.getAddress().getHostString(), server.getAddress().getPort());
        LoggerConfig.getApplicationLogger().info(() -> String.format("Server started on %s !!", baseUrl));
        return baseUrl;
    }

    public void stop(int delay) {
        server.stop(delay);
        LoggerConfig.getDebugLogger().info(() -> String.format("Server stopped initiated with delay %s !!", delay));
    }

    public static void main(String[] args) throws IOException {
        LoggerConfig.setupGlobalConsoleLogger();
        var httpPort = Integer.getInteger("PORT", 8080);
        var app = new App(httpPort);
        var baseUrl = app.start();
        LoggerConfig.getApplicationLogger().info(() -> String.format("Server started. Use url: %s !!", baseUrl));
    }
}
